<%--
  Created by IntelliJ IDEA.
  User: nerond-pc
  Date: 16.02.2020
  Time: 18:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <body>
    <%
      if(request.getSession().getAttribute("user")!=null) {
    %>
    <h2> Hello ${req.getSession().getAttribute("user").username} </h2>
    <%
    } else {
    %>
    <h2> Hello anonymous </h2>
    <%
      }
    %>
    <button onclick="location.href='/login'">Login</button>
    <button onclick="location.href='/logout'">Log Out</button>
  </body>
</html>
