

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.json.JSONObject;

public class Main {

  public static class FingerMapper
       extends Mapper<Object, Text, Text, DoubleWritable>{
    private Text word = new Text();
    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      StringTokenizer itr = new StringTokenizer(value.toString());
      while (itr.hasMoreTokens()) {
    	  JSONObject obj = new JSONObject(itr.nextToken());
          String side = obj.getString("side");
          int series = obj.getInt("series");
          JSONObject fingers = obj.getJSONObject("features2D");
          Iterator<String> fingerIter = fingers.keys();
          while(fingerIter.hasNext()) {
    	    String next = fingerIter.next();
	    	word.set(side + "-" + series + " " + next);
            context.write(word, new DoubleWritable(fingers.getDouble(next)));
          }
      }
    }
  }

  public static class FingerReducer
       extends Reducer<Text,DoubleWritable,Text,DoubleWritable> {
    private DoubleWritable result = new DoubleWritable();

    public void reduce(Text key, Iterable<DoubleWritable> values,
                       Context context
                       ) throws IOException, InterruptedException {
      List<Double> valueList = new ArrayList<Double>();
      values.forEach(v -> valueList.add(v.get()));
      double sum = 0;
      int arraySize = 0;
      for (double val : valueList) {
        sum += val;
        arraySize++;
      }
      double mean = sum / arraySize;
      double variation = 0;
      for (double val : valueList) {
    	  variation += Math.pow(val - mean, 2);
	  }
      double stdDev = Math.sqrt(variation / arraySize);
      result.set(stdDev);
      context.write(key, result);
    }
  }

  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    Job job = Job.getInstance(conf, "word count");
    job.setJarByClass(Main.class);
    job.setMapperClass(FingerMapper.class);
    job.setReducerClass(FingerReducer.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(DoubleWritable.class);
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
}